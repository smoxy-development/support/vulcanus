# Start from Ubuntu 20.10 Groovy
FROM ubuntu:20.10

# Prevent command-line prompts when building the container
ARG DEBIAN_FRONTEND=noninteractive

# Update all packages & install required packages from the ubuntu package repository.
# Autoremove and clean to keep the image size small.
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
        git \
        clang \
        clang-11 \
        clang-tidy \
        clang-tools \
        libsdl2-dev \
        libsdl2-image-dev \
        wget \
        ninja-build \
        xvfb \
        doxygen \
        graphviz \
        libxerces-c-dev \
        valgrind gdb \
        libsdl2-ttf-dev \
        libmpg123-dev \
        libsdl2-mixer-dev \
        libasio-dev \
        libsqlite3-dev \
    && \
    apt-get autoremove -y && \
    apt-get clean

# Set the environment variables C and CXX to clang and clang++ respectively.
# This will cause CMake to use Clang with the LLVM infrastructure by default instead of gcc.
ENV CC /usr/bin/clang
ENV CXX /usr/bin/clang++

# Set the environment variables CMAKE_GENERATOR and CMAKE_MAKE_PROGRAM to
# Ninja and the path to the ninja executable respectable to make sure that Ninja is used
# as a build system.
ENV CMAKE_GENERATOR Ninja
ENV CMAKE_MAKE_PROGRAM /usr/bin/ninja

# Setup clang as the default C compiler and clang++ as the default C++ compiler.
RUN update-alternatives --set cc /usr/bin/clang && \
    update-alternatives --set c++ /usr/bin/clang++

# Install CMake from GitHub
RUN wget https://github.com/Kitware/CMake/releases/download/v3.18.3/cmake-3.18.3-Linux-x86_64.sh \
      -q -O /tmp/cmake-install.sh \
      && chmod u+x /tmp/cmake-install.sh \
      && mkdir /usr/bin/cmake \
      && /tmp/cmake-install.sh --skip-license --prefix=/usr/bin/cmake \
      && rm /tmp/cmake-install.sh

# Include CMake in the PATH environment variable.
ENV PATH="/usr/bin/cmake/bin:${PATH}"
