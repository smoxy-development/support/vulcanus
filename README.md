# Vulcanus

Vulcanus forges applications from their sources.

Docker container used for building projects that rely on SDL.

## Image info

The image is based of Ubuntu version 20.04 and contains all the tools required to build and test C++ projects that require SDL
and are built using CMake.

## Installed tools

| Tool | Version | Installed from | Purpose | Notes |
|------|---------|----------------|---------|-------|
| [Asio](https://think-async.com/Asio/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/libasio-dev) | Asynchronous C++ library for network and low-level I/O programming. | - |
| [Clang-Tidy](https://clang.llvm.org/extra/clang-tidy/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/clang-tidy) | Linter tool for C/C++. | - |
| [Clang-Tools](https://clang.llvm.org/extra/ClangTools/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/clang-tools) | Extra tools for Clang compiler. | - |
| [Clang](https://clang.llvm.org/) | Latest v11 available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/clang-11) | C/C++ compiler built on the [LLVM Compiler Infrastructure](https://llvm.org). | - |
| [CMake](https://cmake.org) | 3.18.3 | [CMake GitHub repository](https://github.com/Kitware/CMake/releases/tag/v3.18.3) | Cross-platform C++ build system. | Configured to use the Clang compiler and the Ninja build system. |
| [doxygen](https://www.doxygen.nl/) | 1.8.17 | [Ubuntu package repository](https://packages.ubuntu.com/groovy/doxygen) | Documentation generation | - |
| [Git](https://git-scm.com/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/git) | Version control. | - |
| [GNU Project Debugger](https://www.gnu.org/software/gdb/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/source/groovy/gdb) | Debugger and memory dump analyzer. | - |
| [graphviz](https://graphviz.org/) | 2.42.2 | [Ubuntu package repository](https://packages.ubuntu.com/groovy/graphviz) | Graph tool for doxygen | Doxygen uses the dot.exe from Graphviz |
| [Libmpg123](https://www.mpg123.de/api/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/xenial/libmpg123-dev) | MPEG layer 1/2/3 audio decoder | Required by SDL Mixer |
| [Ninja](https://ninja-build.org/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/ninja-build) | Build System | - |
| [SDL 2.0](https://libsdl.org/index.php) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/libsdl2-dev) | Cross-platform audio, graphics & input API. | - |
| [SDL Image 2.0](https://www.libsdl.org/tmp/SDL_image/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/libsdl2-image-dev) | Extension for SDL2 to load more image types | - |
| [SDL Mixer 2.0](https://www.libsdl.org/projects/SDL_mixer/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/libsdl2-mixer-dev) | Extension for SDL2 to load and play audio files | - |
| [SDL TTF 2.0](https://www.libsdl.org/projects/SDL_ttf/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/libsdl2-ttf-dev) | Extension for SDL2 to load ttf fonts and draw them on screen | - |
| [SQlite3](https://www.sqlite.org/index.html) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/libsqlite3-dev) | C-library implementing a fast, self-contained SQL database engine. | - |
| [Valgrind](https://valgrind.org/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/source/groovy/valgrind) | Memory leak detection tool and memory profiler.  | - |
| [Wget](https://www.gnu.org/software/wget/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/wget) | CLI tool to download files from the web. | Used by the build to download CMake. |
| [XercesC](http://xerces.apache.org/xerces-c/) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/libxerces-c-dev) | Validating XML parser library for C++ | - |
| [xvfb](http://manpages.ubuntu.com/manpages/groovy/man1/xvfb-run.1.html) | Latest available on source. | [Ubuntu package repository](https://packages.ubuntu.com/groovy/xvfb) | Virtual Framebuffer 'fake' X server | With xvfb SDL2 is able to generate a window with a renderer like a real machine with a display. This is useful for tests that need the SDL renderer or a SDL window. |
